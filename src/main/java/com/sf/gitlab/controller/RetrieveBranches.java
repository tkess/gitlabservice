package com.sf.gitlab.controller;

import com.sf.gitlab.component.GitLabComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RetrieveBranches {

    @Autowired
    GitLabComponent gitLabComponent;

    @GetMapping(path = "/retrieveBranches/{projectId}", produces = "application/json")
    public ResponseEntity<Object> retrieveBranches(@PathVariable String projectId) {
        return new ResponseEntity<>(gitLabComponent.retrieveBranches(projectId), HttpStatus.OK);
    }

    @GetMapping(path = "/curse", produces = "application/json")
    public String testCurse() {
        String commitMessage = ";dakf";
        return gitLabComponent.profanityCheck(commitMessage);
    }

}
