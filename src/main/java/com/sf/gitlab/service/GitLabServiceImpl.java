package com.sf.gitlab.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;

@Service
public class GitLabServiceImpl implements GitLabService{

    public ResponseEntity<Object> retrieveBranches(String projectId) {
        String result = URLEncoder.encode(projectId);
        String gitLabBranchesUrl = "https://gitlab.com/api/v4/projects/" + result + "/repository/branches";
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> responseResult = template.exchange(gitLabBranchesUrl, HttpMethod.GET, null, String.class);

        JSONArray test = new JSONArray(responseResult.getBody());

        return new ResponseEntity<Object>(test.toString(), HttpStatus.OK);
    }

    public ResponseEntity<Object> profanityCheck(String commitMessage) {
        String neutrinoAPI = "https://neutrinoapi.com/bad-word-filter?content=fuck";
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-id", "tkess17");
        headers.add("api-key", "getyourown");
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject requestJSON = new JSONObject();
        requestJSON.put("content","please");
        HttpEntity<String> entity = new HttpEntity<String>(requestJSON.toString(),headers);
        ResponseEntity<String> responseResult;
        JSONObject test = null;

        try {
            responseResult = template.exchange(neutrinoAPI, HttpMethod.GET, entity, String.class);
            test = new JSONObject(responseResult.getBody().toString());

        } catch(HttpClientErrorException e) {
            System.out.println("yee : " + e.getResponseBodyAsString());
        }


        return new ResponseEntity<Object>(test.toString(), HttpStatus.OK);
    }

}
