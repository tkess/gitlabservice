package com.sf.gitlab.service;

import org.springframework.http.ResponseEntity;

public interface GitLabService {

    public ResponseEntity<Object> retrieveBranches(String projectId);

    public ResponseEntity<Object> profanityCheck(String commitMessage);

}
