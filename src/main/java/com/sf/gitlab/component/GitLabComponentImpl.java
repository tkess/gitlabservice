package com.sf.gitlab.component;

import org.apache.coyote.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.sf.gitlab.service.GitLabService;

import java.util.ArrayList;
import java.util.List;

@Component
public class GitLabComponentImpl implements GitLabComponent {

    @Autowired
    GitLabService gitLabService;

    public List<String> retrieveBranches(String projectId) {
        ResponseEntity<Object> branchDetails = gitLabService.retrieveBranches(projectId);
        String responseBody = branchDetails.getBody().toString();
        System.out.println(responseBody);
        return parseBranchNames(responseBody);
    }

    private List<String> parseBranchNames(String responseBody) {
        JSONArray responseArray = new JSONArray(responseBody);
        JSONObject tempJSON = null;
        List<String> branchNames = new ArrayList<>();
        for(int i = 0; i < responseArray.length(); i++) {
            tempJSON = responseArray.getJSONObject(i);
            branchNames.add(tempJSON.getString("name"));
        }

        return branchNames;
    }

    public String profanityCheck(String commitMessage) {
        ResponseEntity<Object> response = gitLabService.profanityCheck(commitMessage);
        String responseBody = response.getBody().toString();
        return responseBody;
    }

}
