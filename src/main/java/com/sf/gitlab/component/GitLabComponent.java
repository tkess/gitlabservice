package com.sf.gitlab.component;

import java.util.List;

public interface GitLabComponent {

    public List<String> retrieveBranches(String projectId);

    public String profanityCheck(String commitMessage);

}
